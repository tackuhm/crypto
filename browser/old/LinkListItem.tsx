import * as React from 'react';
import { FC } from 'react';
import { Link, ReactRouteRecord } from 'rocon/react';

type Props = {
  route: ReactRouteRecord<Record<string, unknown>>;
  label: string;
  selected: boolean;
};

export const LinkListItem: FC<Props> = ({ route, label, selected }) => {
  if (selected) {
    return <li>{label}</li>;
  } else {
    return (
      <li>
        <Link {...{ route }}>{label}</Link>
      </li>
    );
  }
};
