import * as React from 'react';
import { FC, useState } from 'react';

import { Dex } from '../models';
import { DataTable } from './DataTable';

const dexes: Array<Dex> = [
  { name: 'All' },
  { name: 'PancakeSwap' },
  { name: 'HogehogeSwap' },
];

export const Contracts: FC = () => {
  const [selectedDexId, setSelectedDexId] = useState<number>(0);

  return (
    <>
      <select
        onChange={(e) => {
          setSelectedDexId(parseInt(e.target.value));
        }}
      >
        {dexes.map((dex: Dex, idx: number) => {
          return (
            <option selected={idx === selectedDexId} value={idx}>
              {dex.name}
            </option>
          );
        })}
      </select>
      <p>selectedDexId: {selectedDexId}</p>
      <DataTable selectedDex={dexes[selectedDexId]} />
    </>
  );
};
