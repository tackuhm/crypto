import { style } from '@vanilla-extract/css';

export const className = style({
  borderRadius: '58px',
  marginBottom: '20px',
});
