import * as React from 'react';
import { FC } from 'react';

import { className } from './DataTable.css';

import { Dex } from '../models';

type Props = {
  selectedDex: Dex;
};

export const DataTable: FC<Props> = ({ selectedDex }) => {
  return (
    <table>
      <tr {...{ className }}>
        <th>ChainId</th>
        <th>DEX</th>
        <th>ContractType</th>
        <th>Address</th>
      </tr>
      <tr {...{ className }}>
        <td>56</td>
        <td>PancakeSwap</td>
        <td>MasterChef</td>
        <td>0x73feaa1ee314f8c655e354234017be2193c9e24e</td>
      </tr>
      <tr {...{ className }}>
        <td>56</td>
        <td>PancakeSwap</td>
        <td>Router</td>
        <td>0x10ed43c718714eb63d5aa57b78b54704e256024e</td>
      </tr>
      <tr {...{ className }}>
        <td>56</td>
        <td>PancakeSwap</td>
        <td>Token</td>
        <td>0x0e09fabb73bd3ade0a17ecc321fd13a19e81ce82</td>
      </tr>
      <tr {...{ className }}>
        <td>56</td>
        <td>HogehogeSwap</td>
        <td>MasterChef</td>
        <td>0xaaaaaaa</td>
      </tr>
      <tr {...{ className }}>
        <td>56</td>
        <td>HogehogeSwap</td>
        <td>Router</td>
        <td>0xbbbbbbb</td>
      </tr>
      <tr {...{ className }}>
        <td>56</td>
        <td>HogehogeSwap</td>
        <td>Token</td>
        <td>0xccccccc</td>
      </tr>
    </table>
  );
};
