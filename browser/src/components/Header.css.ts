import { style } from '@vanilla-extract/css';

export const className = style({
  // fontFamily: ['M PLUS 1p', 'sans-serif'],
  fontSize: '24px',
  fontWeight: 'bold',
  color: '#FDFFFC',
  height: '60px',
  background: '#005377',
  marginBottom: '30px',
  paddingTop: '40px',
  paddingLeft: '40px',
});
