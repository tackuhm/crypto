import * as React from 'react';
import { FC } from 'react';

import { className } from './Header.css';

export const Header: FC = () => {
  return <header {...{ className }}>tackuhm block chain database</header>;
};
