import { style } from '@vanilla-extract/css';
import { calc } from '@vanilla-extract/css-utils';

export const classNames = {
  container: style({
    height: '700px',
    display: 'flex',
  }),
  nav: style({
    width: '240px',
  }),
  main: style({
    width: calc.subtract('100%', '240px'),
  }),
  footer: style({
    height: '300px',
  }),
};
