import * as React from 'react';
import { FC } from 'react';

import { classNames } from './Layout.css';

import { Header } from './Header';
import { Contracts } from './Contracts';

export const createLayout: (arg: { sidebar: () => JSX.Element }) => FC<{}> = ({
  sidebar,
}) => ({}) => {
  return (
    <>
      <Header />
      <div className={classNames.container}>
        <nav className={classNames.nav}>{sidebar()}</nav>
        <main className={classNames.main}>
          <Contracts />
        </main>
      </div>
      <footer className={classNames.footer}>Footer</footer>
    </>
  );
};
