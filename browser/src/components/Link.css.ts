import { style } from '@vanilla-extract/css';

export const className = style({
  textDecoration: 'none',
  fontWeight: 'bold',
  selectors: {
    '&:link': {
      color: '#005377',
    },
    '&:visited': {
      color: '#005377',
    },
  },
});
