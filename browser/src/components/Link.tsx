import * as React from 'react';
import { FC } from 'react';

import { ReactRouteRecord, Link as RoconLink } from 'rocon/react';

import { className } from './Link.css';

export type Props = {
  label: string;
  selected: boolean;
  route: ReactRouteRecord<Record<string, unknown>> | string;
};

export const Link: FC<Props> = ({ selected, label, route }) => {
  if (selected) {
    return <>{label}</>;
  } else if (typeof route === 'string') {
    return (
      <a href="" {...{ className }}>
        {label}
      </a>
    );
  } else {
    return <RoconLink {...{ route, className }}>{label}</RoconLink>;
  }
};
