import { style } from '@vanilla-extract/css';

export const className = style({
  listStyle: 'none',
});

export const notSelectedListItem = style({
  marginBottom: '8px',
});

export const selectedListItem = style({
  marginBottom: '8px',
  textDecoration: 'none',
  fontWeight: 'bold',
  background: '#005377',
  color: 'white',
});
