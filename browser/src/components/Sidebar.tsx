import * as React from 'react';
import { FC } from 'react';

import { Link } from './Link';
import { ReactRouteRecord } from 'rocon/react';

import {
  className,
  notSelectedListItem,
  selectedListItem,
} from './Sidebar.css';

export type Props = {
  selectedKey?: string;
  items: Array<{
    key: string;
    label: string;
    route: ReactRouteRecord<Record<string, unknown>> | string;
  }>;
};

export const Sidebar: FC<Props> = ({ items, selectedKey }) => {
  return (
    <ul {...{ className }}>
      {items.map((i) => {
        const { label, route, key } = i;
        const selected = key === selectedKey;
        if (selected) {
          return (
            <li className={selectedListItem} {...{ key }}>
              <Link {...{ selected, label, route }} />
            </li>
          );
        } else {
          return (
            <li className={notSelectedListItem} {...{ key }}>
              <Link {...{ selected, label, route }} />
            </li>
          );
        }
      })}
    </ul>
  );
};
