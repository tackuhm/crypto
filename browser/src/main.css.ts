import { globalStyle } from '@vanilla-extract/css';

globalStyle('html, body', {
  margin: 0,
  padding: 0,
  fontFamily: ['Nanum Gothic Coding'],
  background: '#FDFFFC',
  fontSize: '20px',
});
