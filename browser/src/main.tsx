import * as React from 'react';
import { FC } from 'react';
import * as ReactDOM from 'react-dom';

import { RoconRoot } from 'rocon/react';
import { Routes } from './routes';

import { Layout } from './components/Layout';

import './main.css';

const App: FC = () => {
  return (
    <RoconRoot>
      <Routes />
    </RoconRoot>
  );
};

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
