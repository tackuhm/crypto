import React from 'react';
import { FC } from 'react';
import { Path, useRoutes } from 'rocon/react';

import { Sidebar, Props as SidebarProps } from './components/Sidebar';
import { createLayout } from './components/Layout';

const toplevelRoutes = Path().route('chains').route('dexes').route('contracts');

const items = [
  { label: 'Chains', key: 'chains', route: toplevelRoutes._.chains },
  { label: 'Dexes', key: 'dexes', route: toplevelRoutes._.dexes },
  { label: 'Contracts', key: 'contracts', route: toplevelRoutes._.contracts },
];

toplevelRoutes.exact({
  action: () => {
    return createLayout({ sidebar: () => <Sidebar {...{ items }} /> });
  },
});
toplevelRoutes._.chains.attach(Path()).exact({
  action: () => {
    const selectedKey = 'chains';
    return <Layout sidebar={{ ...{ selectedKey, items } }} />;
  },
});
toplevelRoutes._.dexes.attach(Path()).exact({
  action: () => {
    const selectedKey = 'dexes';
    return <Layout sidebar={{ ...{ selectedKey, items } }} />;
  },
});
toplevelRoutes._.contracts.attach(Path()).exact({
  action: () => {
    const selectedKey = 'contracts';
    return <Layout sidebar={{ ...{ selectedKey, items } }} />;
  },
});

// .route('chains', (route) => route.action(() => <Layout />))
// .route('dexes', (route) => route.action(() => <Layout />))

export const Routes: React.FC = () => {
  return useRoutes(toplevelRoutes);
};
