import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { Sidebar } from '../components/Sidebar';

export default {
  title: 'Default/Sidebar',
  component: Sidebar,
  argTypes: {
    backgroundColor: { control: 'color' },
  },
} as ComponentMeta<typeof Sidebar>;

const Template: ComponentStory<typeof Sidebar> = (args) => (
  <Sidebar {...args} />
);

const items = [{ label: 'Chains' }, { label: 'Dexes' }, { label: 'Contracts' }];

export const NotSelected = Template.bind({});
NotSelected.args = {
  items: [
    { label: 'Chains', selected: false, route: '' },
    { label: 'Dexes', selected: false, route: '' },
    { label: 'Contracts', selected: false, route: '' },
  ],
};

export const Selected = Template.bind({});
Selected.args = {
  items: [
    { label: 'Chains', selected: false, route: '' },
    { label: 'Dexes', selected: true, route: '' },
    { label: 'Contracts', selected: false, route: '' },
  ],
};
